package com.company.repositories;

import com.company.models.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserProfile, Integer> {
    UserProfile findUserByUsername(String username);
}