package com.company.services;

import com.company.models.Employee;
import com.company.repositories.EmployeeRepository;
import com.company.services.interfaces.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getById(int id) {
        ArrayList<Employee> result = new ArrayList<>();
        employeeRepository.findById(id).ifPresent(result::add);
        return result.get(0);
    }

    @Override
    public void create(Employee employee) {
        employeeRepository.save(employee);
    }
}
