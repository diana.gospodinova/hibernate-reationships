package com.company.services;

import com.company.models.Tag;
import com.company.repositories.TagRepository;
import com.company.services.interfaces.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }
    @Override
    public List<Tag> getAll() {
        return tagRepository.findAll();
    }

    @Override
    public Tag getById(Integer id) {
        ArrayList<Tag> result = new ArrayList<>();
        tagRepository.findById(id).ifPresent(result::add);
        return result.get(0);
    }

    @Override
    public Tag create(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public Tag update(Integer id, Tag tag) {
        return tagRepository.save(tag);
    }
}
