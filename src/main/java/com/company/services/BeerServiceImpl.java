package com.company.services;

import com.company.models.Beer;
import com.company.models.Tag;
import com.company.repositories.BeerRepository;
import com.company.repositories.TagRepository;
import com.company.services.interfaces.BeerService;
import com.company.services.interfaces.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BeerServiceImpl implements BeerService {
    private BeerRepository beerRepository;
    private TagRepository tagRepository;
    private TagService tagService;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository, TagRepository tagRepository, TagService tagService) {
        this.beerRepository = beerRepository;
        this.tagRepository = tagRepository;
        this.tagService = tagService;
    }

    @Override
    public List<Beer> getAll() {
        return beerRepository.findAll();
    }

    @Override
    public Beer getById(Integer id) {
        ArrayList<Beer> result = new ArrayList<>();
        beerRepository.findById(id).ifPresent(result::add);
        return result.get(0);
    }

    @Override
    public List<Tag> getTagsById(Integer id) {
        return null;
    }

    @Override
    public Beer create(Beer beer) {
        beer.setTags(tagRepository.findAll());
        return beerRepository.save(beer);
    }

    @Override
    public Beer update(Integer id, Beer beer) {
        return beerRepository.save(beer);
    }

    @Override
    public void update(Integer id, Integer tagId) {
        Beer beer = getById(id);
       /* Tag tag = tagService.getById(tagId);
        beer.setTags(Arrays.asList(tag));*/
        beer.setName("New beer name");
        beerRepository.save(beer);
    }
}
