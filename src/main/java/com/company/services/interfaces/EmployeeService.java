package com.company.services.interfaces;

import com.company.models.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getAll();

    Employee getById(int id);

    void create(Employee employee);
}
