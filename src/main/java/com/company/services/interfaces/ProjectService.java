package com.company.services.interfaces;

import com.company.models.Employee;
import com.company.models.Project;

import java.util.List;

public interface ProjectService {
    List<Project> getAll();

    Project getById(int id);
}
