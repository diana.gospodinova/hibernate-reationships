package com.company.services.interfaces;

import com.company.models.UserDTO;
import com.company.models.UserProfile;

public interface UserService {
    void create(UserDTO user);

    UserProfile getDetails(String username);
}
