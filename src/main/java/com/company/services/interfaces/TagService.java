package com.company.services.interfaces;

import com.company.models.Tag;

import java.util.List;

public interface TagService {
    List<Tag> getAll();

    Tag getById(Integer id);

    Tag create(Tag tag);

    Tag update(Integer id, Tag tag);
}
