package com.company.services.interfaces;

import com.company.models.Beer;
import com.company.models.Tag;

import java.util.List;

public interface BeerService {
    List<Beer> getAll();

    Beer getById(Integer id);

    List<Tag> getTagsById(Integer id);

    Beer create(Beer beer);

    Beer update(Integer id, Beer beer);

    void update(Integer id, Integer tagId);
}
