package com.company.services;

import com.company.models.Project;
import com.company.repositories.ProjectRepository;
import com.company.services.interfaces.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {
    private ProjectRepository projectRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> getAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project getById(int id) {
        ArrayList<Project> result = new ArrayList<>();
        projectRepository.findById(id).ifPresent(result::add);
        return result.get(0);
    }
}
