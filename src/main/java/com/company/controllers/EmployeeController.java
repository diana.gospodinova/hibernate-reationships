package com.company.controllers;

import com.company.models.Employee;
import com.company.models.Project;
import com.company.services.interfaces.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<Employee> getAll() {
        return employeeService.getAll();
    }

    @GetMapping("/{id}")
    public Employee getById(@PathVariable int id) {
        return employeeService.getById(id);
    }

    @GetMapping("/{id}/projects")
    public List<Project> getProjects(@PathVariable int id) {
        return employeeService.getById(id).getProjects();
    }

    @PostMapping
    public Employee create(@Valid @RequestBody Employee employee) {
        employeeService.create(employee);
        return employee;
    }
}
