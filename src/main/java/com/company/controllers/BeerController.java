package com.company.controllers;

import com.company.models.Beer;
import com.company.models.Tag;
import com.company.services.BeerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/beers")
public class BeerController {
    private BeerServiceImpl beerService;

    @Autowired
    public BeerController(BeerServiceImpl beerService) {
        this.beerService = beerService;
    }

    @GetMapping
    public List<Beer> getAll() {
        return beerService.getAll();
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable Integer id) {
        return beerService.getById(id);
    }

    @GetMapping("/{id}/tags")
    public List<Tag> getTagsById(@PathVariable Integer id) {
        return beerService.getById(id).getTags();
}

    @PostMapping("/new")
    public Beer create(@RequestBody Beer beer) {
        beerService.create(beer);
        return beer;
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable Integer id, @RequestBody Beer beer) {
        beerService.update(id, beer);
        return beer;
    }

    @PutMapping("/{id}/{tagId}")
    public void tagBeer(@PathVariable Integer id, @PathVariable Integer tagId) {
        beerService.update(id, tagId);
    }
}
