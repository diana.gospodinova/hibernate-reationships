package com.company.controllers;

import com.company.models.Tag;
import com.company.services.TagServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/tags")
public class TagController {
    private TagServiceImpl tagService;

    @Autowired
    public TagController(TagServiceImpl tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    public List<Tag> getAll() {
        return tagService.getAll();
    }

    @GetMapping("/{id}")
    public Tag getById(@PathVariable Integer id) {
        return tagService.getById(id);
    }

    @PostMapping("/new")
    public Tag create(@RequestBody Tag tag) {
        tagService.create(tag);
        return tag;
    }

    @PutMapping("/{id}")
    public Tag update(@PathVariable Integer id, @RequestBody Tag tag) {
        tagService.update(id, tag);
        return tag;
    }
}
