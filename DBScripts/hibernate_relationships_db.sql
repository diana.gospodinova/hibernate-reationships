-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.5.0-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дъмп на структурата на БД hibernate_relationships_db
DROP DATABASE IF EXISTS `hibernate_relationships_db`;
CREATE DATABASE IF NOT EXISTS `hibernate_relationships_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `hibernate_relationships_db`;

-- Дъмп структура за таблица hibernate_relationships_db.authorities
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(15) NOT NULL,
  `authority` varchar(50) NOT NULL,
  KEY `authorities_users_username_fk` (`username`),
  CONSTRAINT `authorities_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дъмп данни за таблица hibernate_relationships_db.authorities: ~2 rows (приблизително)
DELETE FROM `authorities`;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` (`username`, `authority`) VALUES
	('username', 'ROLE_USER'),
	('user2', 'ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;

-- Дъмп структура за таблица hibernate_relationships_db.beers
DROP TABLE IF EXISTS `beers`;
CREATE TABLE IF NOT EXISTS `beers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abv` double DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица hibernate_relationships_db.beers: 4 rows
DELETE FROM `beers`;
/*!40000 ALTER TABLE `beers` DISABLE KEYS */;
INSERT INTO `beers` (`id`, `abv`, `description`, `name`) VALUES
	(1, 4.5, 'The first beer', 'New beer name'),
	(2, 4.5, 'The second beer', 'Beer 2'),
	(3, 4.5, 'The third beer', 'Beer 3'),
	(4, 4.5, 'The third beer', 'Beer 3');
/*!40000 ALTER TABLE `beers` ENABLE KEYS */;

-- Дъмп структура за таблица hibernate_relationships_db.beers_tags
DROP TABLE IF EXISTS `beers_tags`;
CREATE TABLE IF NOT EXISTS `beers_tags` (
  `beer_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`beer_id`,`tag_id`),
  KEY `FKln663oh0qbx16ly5qdl4h4bbi` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица hibernate_relationships_db.beers_tags: 3 rows
DELETE FROM `beers_tags`;
/*!40000 ALTER TABLE `beers_tags` DISABLE KEYS */;
INSERT INTO `beers_tags` (`beer_id`, `tag_id`) VALUES
	(1, 5),
	(4, 1),
	(4, 2);
/*!40000 ALTER TABLE `beers_tags` ENABLE KEYS */;

-- Дъмп структура за таблица hibernate_relationships_db.employees
DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица hibernate_relationships_db.employees: 2 rows
DELETE FROM `employees`;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` (`id`, `name`) VALUES
	(1, 'Employee 2'),
	(2, 'Employee new');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Дъмп структура за таблица hibernate_relationships_db.employees_projects
DROP TABLE IF EXISTS `employees_projects`;
CREATE TABLE IF NOT EXISTS `employees_projects` (
  `employee_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  KEY `FKc9x9r7nbtwnx24vcrfxym9cyy` (`project_id`),
  KEY `FKbslohhow39ayel1dc6cdus5sc` (`employee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица hibernate_relationships_db.employees_projects: 2 rows
DELETE FROM `employees_projects`;
/*!40000 ALTER TABLE `employees_projects` DISABLE KEYS */;
INSERT INTO `employees_projects` (`employee_id`, `project_id`) VALUES
	(1, 1),
	(2, 1);
/*!40000 ALTER TABLE `employees_projects` ENABLE KEYS */;

-- Дъмп структура за таблица hibernate_relationships_db.projects
DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица hibernate_relationships_db.projects: 2 rows
DELETE FROM `projects`;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `name`) VALUES
	(1, 'Project 1'),
	(2, 'Project 2');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;

-- Дъмп структура за таблица hibernate_relationships_db.tags
DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица hibernate_relationships_db.tags: 5 rows
DELETE FROM `tags`;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`tag_id`, `name`) VALUES
	(1, 'Tag 1'),
	(2, 'Tag 2'),
	(3, 'Tag 3'),
	(4, 'Tag 4'),
	(5, 'Tag 5');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Дъмп структура за таблица hibernate_relationships_db.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `middle_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `password` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дъмп данни за таблица hibernate_relationships_db.users: ~2 rows (приблизително)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `email`, `first_name`, `middle_name`, `last_name`, `enabled`, `password`) VALUES
	(4, 'username', NULL, 'First Name', NULL, 'Last Name', 1, '$2a$10$UD4xfHvkY.v6YJzHlnq/Jes96m79Oe6meeHhoCNW4conqDzwHuHFu'),
	(5, 'user2', NULL, 'First Name', NULL, 'Last Name', 1, '$2a$10$KBdCrLTBJ5fcPbCp1VSv7uXp0Bs2iS5fBEipccG9DRofGflPvt8Yi');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
